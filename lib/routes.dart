import 'package:chang_exam/week2/miso/screen_miso_main.dart';
import 'package:chang_exam/week2/starbucks/screen_sb_main.dart';
import 'package:chang_exam/week3/screen_diary.dart';
import 'package:chang_exam/week4/screen_shazam_home.dart';
import 'package:chang_exam/week5/screen_toss_main.dart';
import 'package:chang_exam/week6/screen_book_store.dart';
import 'package:flutter/material.dart';

final routes = {
  '/': (BuildContext context) => BookStoreScreen(),
  '/miso': (BuildContext context) => MisoMainScreen(),
  '/starBucks': (BuildContext context) => StarBucksMainScreen(),
  '/singleLineDiary': (BuildContext context) => DiaryScreen(),
  '/shazam': (BuildContext context) => ShazamHomeScreen(),
  '/toss': (BuildContext context) => TossMainScreen(),
  '/bookStore': (BuildContext context) => BookStoreScreen(),
};
