import 'package:flutter/material.dart';

class PageIndexScreen extends StatelessWidget {
  const PageIndexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '부트 캠프 창',
          style: TextStyle(color: Colors.black),
        ),
        elevation: 1,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              PageNavigatorWidget(page: '/miso', title: 'Miso'),
              PageNavigatorWidget(page: '/starBucks', title: 'StarBucks'),
              PageNavigatorWidget(page: '/singleLineDiary', title: '한 줄 일기'),
              PageNavigatorWidget(page: '/shazam', title: 'Shazma'),
              PageNavigatorWidget(page: '/toss', title: '토스'),
              PageNavigatorWidget(page: '/bookStore', title: 'BookStore'),
            ],
          ),
        ),
      ),
    );
  }
}

class PageNavigatorWidget extends StatelessWidget {
  String page;
  String title;
  PageNavigatorWidget({Key? key, required this.page, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, page);
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              Icon(Icons.arrow_forward_ios)
            ],
          ),
        ),
      ),
    );
  }
}
