import 'package:chang_exam/week2/starbucks/screen_sb_home.dart';
import 'package:chang_exam/week2/starbucks/order/screen_sb_order.dart';
import 'package:chang_exam/week2/starbucks/pay/screen_sb_pay.dart';
import 'package:flutter/material.dart';

class StarBucksMainScreen extends StatefulWidget {
  const StarBucksMainScreen({Key? key}) : super(key: key);

  @override
  _StarBucksMainScreenState createState() => _StarBucksMainScreenState();
}

class _StarBucksMainScreenState extends State<StarBucksMainScreen> {
  int _currentPageIndex = 0;
  final List<Widget> _pageList = [
    StarBucksHome(),
    StarBucksPay(),
    StarBucksOrder(),
    StarBucksHome(),
    StarBucksPay(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _currentPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _pageList[_currentPageIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list,
            ),
            label: 'List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_cafe),
            label: 'Gift',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.card_giftcard,
            ),
            label: 'Info',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.more_horiz,
            ),
            label: 'Info',
          ),
        ],
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _currentPageIndex,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      ),
    );
  }
}
