import 'package:flutter/material.dart';

class HanBul extends StatefulWidget {
  const HanBul({Key? key}) : super(key: key);

  @override
  _HanBulState createState() => _HanBulState();
}

class _HanBulState extends State<HanBul> {
  var starbucksAccentColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverAppBar(
                automaticallyImplyLeading: false, // 뒤로가기 버튼 숨기기
                pinned: true,
                snap: false, // 중간에 멈춰도 자동으로 AppBar가 펼쳐지지 않도록
                floating: true,
                expandedHeight: 250,
                backgroundColor: Colors.white, // 최대 확장되었을 때의 높이
                // 스크롤 시 사라지는 영역
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.pin,
                  background: Stack(
                    children: [
                      Positioned.fill(
                        child: Image.network(
                          'https://i.ibb.co/2Pz33q7/2021-12-16-12-21-42-cleanup.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      // 배경 위 위젯들 올리면서 시작점 다르게 하려고 Stack 내에서 Positioned 활용함
                      Positioned(
                        left: 20,
                        right: 20,
                        top: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '한 해의 마무리,\n수고 많았어요:반짝이는_하트:',
                              style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 35,
                            ),
                            Row(
                              children: [
                                // Expanded 사용: Column의 가로 길이를 Row의 남은 자리만큼만 차지하도록 하기 위해
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '11 ★ until next Reward',
                                        style: TextStyle(
                                            color: starbucksAccentColor,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 15),
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                        child: LinearProgressIndicator(
                                          backgroundColor:
                                              Colors.grey.withOpacity(0.3),
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  starbucksAccentColor),
                                          minHeight: 15,
                                          value: 1 / 12,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(width: 15),
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    style: TextStyle(
                                      // RichText는 기본이 흰색이라 안 보임
                                      color: Colors.black,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: '1',
                                        style: TextStyle(
                                            fontSize: 45,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      TextSpan(
                                        text: ' / ',
                                        style: TextStyle(
                                          fontSize: 35,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      TextSpan(
                                        text: '12 ★',
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: starbucksAccentColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                /// 스크롤시 남아있는 영역
                /// SliverAppBar의 bottom은 PreferredSize 위젯으로 시작해야만 함.
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(60),
                  child: Row(
                    children: [
                      Expanded(
                        child: ListTile(
                          leading: Icon(
                            Icons.mail_outline,
                            color: Colors.grey,
                          ),
                          title: Text(
                            'What’s New',
                            style: TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Expanded(
                        child: ListTile(
                          leading: Icon(
                            Icons.confirmation_num_outlined,
                            color: Colors.grey,
                          ),
                          title: Text(
                            'Coupon',
                            style: TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                      //Alarm 넣어야 함
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
