import 'package:flutter/material.dart';

class StarRewardPercent extends StatelessWidget {
  const StarRewardPercent({
    Key? key,
    required double progressValue,
  })  : _progressValue = progressValue,
        super(key: key);

  final double _progressValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '11 until next Reward',
              style: TextStyle(
                  color: Color(0xFFc7b079),
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 8.0,
            ),
            SizedBox(
              width: 200,
              height: 8,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: LinearProgressIndicator(
                  value: _progressValue,
                  backgroundColor: Colors.grey,
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFc7b079)),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          width: 16.0,
        ),
        Text(
          "1",
          style: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.w800,
          ),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          "/",
          style: TextStyle(fontSize: 28, color: Colors.grey),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          "12",
          style: TextStyle(
              fontSize: 28,
              color: Color(0xFFc7b079),
              fontWeight: FontWeight.w700),
        ),
        Icon(
          Icons.star,
          color: Color(0xFFc7b079),
          size: 40,
        )
      ],
    );
  }
}
