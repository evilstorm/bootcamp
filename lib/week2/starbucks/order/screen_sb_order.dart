import 'package:chang_exam/week2/starbucks/order/order_all.dart';
import 'package:chang_exam/week2/starbucks/order/order_cake.dart';
import 'package:chang_exam/week2/starbucks/order/order_my.dart';
import 'package:flutter/material.dart';

class StarBucksOrder extends StatelessWidget {
  const StarBucksOrder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ))
          ],
          bottom: TabBar(
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.black,
            tabs: [
              Tab(
                text: '전체 메뉴',
              ),
              Tab(
                text: '나만의 메뉴',
              ),
              Tab(
                text: '🎂 홈 케이크',
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            'Order',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w800),
          ),
        ),
        body: TabBarView(
          children: [OrderALl(), OrderMy(), OrderCake()],
        ),
      ),
    );
  }
}
