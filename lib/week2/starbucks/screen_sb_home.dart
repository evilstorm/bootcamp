import 'package:chang_exam/week2/starbucks/sliver_header_warpper.dart';
import 'package:chang_exam/week2/starbucks/widget_start_reward.dart';
import 'package:flutter/material.dart';

class StarBucksHome extends StatefulWidget {
  const StarBucksHome({Key? key}) : super(key: key);

  @override
  State<StarBucksHome> createState() => _StarBucksHomeState();
}

class _StarBucksHomeState extends State<StarBucksHome> {
  double _progressMax = 12;
  double _progressValue = 0.2;

  List<dynamic> recommandMenus = [
    {
      "name": "돌체쿠키라떼",
      "imgUrl": "https://i.ibb.co/SwGPpzR/9200000003687-20211118142543832.jpg",
    },
    {
      "name": "아이스 홀리데이\n돌체 쿠키 라떼",
      "imgUrl": "https://i.ibb.co/JHVXZ72/9200000003690-20211118142702357.jpg",
    },
    {
      "name": "스노우 민트\n초콜릿",
      "imgUrl": "https://i.ibb.co/M91G17c/9200000003693-20211118142933650.jpg",
    },
    {
      "name": "아이스 스노우\n민트 초콜릿",
      "imgUrl": "https://i.ibb.co/jyZK4C9/9200000003696-20211118143125337.jpg",
    },
    {
      "name": "스노우 민트\n초콜릿 블렌디드",
      "imgUrl": "https://i.ibb.co/DKkV0rw/9200000003699-20211118143249044.jpg",
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color(0xFF54b88a),
        onPressed: () {},
        label: const Text('Deleverys'),
        icon: Icon(Icons.content_copy_outlined),
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: false,
            floating: false,
            snap: false,
            backgroundColor: Colors.white,
            expandedHeight: 250,
            flexibleSpace: FlexibleSpaceBar(
              // title: reward_section(),
              background: header(),
            ),
          ),
          SliverPersistentHeader(
            floating: true,
            pinned: true,
            delegate: SliverHeader(
                child: reward_section(), minHeight: 50, maxHeight: 50),
          ),
          SliverToBoxAdapter(
            child: Column(
              children: [
                // reward_section(),
                SizedBox(
                  height: 8.0,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: Image.network(
                          'https://i.ibb.co/QcVn97y/2021-12-16-1-33-11.png')),
                ),
                recommanded_section(context),
                SizedBox(
                  height: 16.0,
                ),
                Image.network(
                    'https://i.ibb.co/Fb0q43T/IMG-F9-BA5-CBCB476-1.jpg'),
              ],
            ),
          )
        ],
      ),
    );
  }

  Column recommanded_section(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 24.0, 16.0, 16.0),
          child: RichText(
            text: TextSpan(
                text: '이진호',
                style: TextStyle(
                    fontSize: 18,
                    color: Color(0xFFc7b079),
                    fontWeight: FontWeight.w700),
                children: [
                  TextSpan(
                      text: '님을 위한 추천메뉴',
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black,
                          fontWeight: FontWeight.w700))
                ]),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 150,
            child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(
                width: 16.0,
              ),
              scrollDirection: Axis.horizontal,
              itemCount: recommandMenus.length,
              itemBuilder: (context, index) {
                return menuItem(recommandMenus[index]['imgUrl'],
                    recommandMenus[index]['name']);
              },
            ),
          ),
        ),
      ],
    );
  }

  Container reward_section() {
    return Container(
      color: Colors.white,
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          iconWithText(Icons.mail_outline, "What's News"),
          iconWithText(Icons.confirmation_num_outlined, 'Coupon'),
          Icon(Icons.notification_important),
        ],
      ),
    );
  }

  Container header() {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Image.network(
              'https://i.ibb.co/2Pz33q7/2021-12-16-12-21-42-cleanup.png'),
          Positioned.fill(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                  '한 해의 마무리,\n수고 많으셨어요.',
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.w800),
                ),
              ),
            ),
          ),
          Positioned.fill(
              child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: StarRewardPercent(progressValue: _progressValue),
          ))
        ],
      ),
    );
  }

  Column menuItem(String url, String name) {
    return Column(
      children: [
        CircleAvatar(
          radius: 48,
          backgroundColor: Colors.transparent,
          child: ClipOval(
            child: Image.network(
              url,
            ),
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Text(
          name,
          textAlign: TextAlign.center,
        )
      ],
    );
  }

  Row iconWithText(IconData icon, String text) {
    return Row(
      children: [
        Icon(
          icon,
          color: Colors.grey,
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          text,
          style: TextStyle(fontSize: 16.0),
        )
      ],
    );
  }
}
