import 'package:flutter/material.dart';

class StarBucksPay extends StatelessWidget {
  const StarBucksPay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController viewPagerController =
        PageController(viewportFraction: 0.8);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Pay',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w800),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.list,
              color: Colors.grey,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: PageView(
              pageSnapping: true,
              controller: viewPagerController,
              children: [
                payItem(),
                payItem(),
                payItem(),
              ],
            ),
          ),
          bottomMenu(context)
        ],
      ),
    );
  }

  Padding payItem() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Image.network('https://i.ibb.co/BgfYHg4/2021-12-16-1-49-51.png'),
      ),
    );
  }

  Container bottomMenu(BuildContext context) {
    const double height = 80.0;
    const double indent = (height / 2) - 6;
    return Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      color: Colors.white,
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Center(
                child: Text(
                  'Coupon',
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
            ),
            VerticalDivider(
              color: Colors.grey,
              thickness: 2,
              indent: indent,
              endIndent: indent,
            ),
            Expanded(
              child: Center(
                child: Text(
                  'e-Gift Item',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
