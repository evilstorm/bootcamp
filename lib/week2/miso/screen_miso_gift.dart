import 'package:flutter/material.dart';

class MisoGiftScreen extends StatelessWidget {
  const MisoGiftScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            color: Color(0xFF2864ec),
            image: DecorationImage(
              alignment: Alignment.bottomCenter,
              fit: BoxFit.fitWidth,
              image: NetworkImage(
                'https://i.ibb.co/rxzkRTD/146201680-e1b73b36-aa1e-4c2e-8a3a-974c2e06fa9d.png',
              ),
            )),
        child: Column(
          children: [
            SizedBox(
              height: 86,
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                TextSpan(
                    text: '친구 추천할 때마다\n',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w400)),
                TextSpan(
                    text: '10,000원',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w700)),
                TextSpan(
                    text: ' 할인 쿠폰 지급!',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w400)),
              ]),
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '자세히 보기',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: 12,
                )
              ],
            ),
            Spacer(),
            ElevatedButton.icon(
              onPressed: () {},
              icon: Icon(
                Icons.redeem,
                color: Colors.blueAccent,
              ),
              label: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                child: Text(
                  '친구 추천하기',
                  style: TextStyle(color: Colors.blueAccent),
                ),
              ),
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  primary: Colors.white),
            ),
            SizedBox(
              height: 32.0,
            )
          ],
        ));
  }
}
