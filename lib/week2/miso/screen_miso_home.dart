import 'package:flutter/material.dart';

class MisoHomeScreen extends StatelessWidget {
  const MisoHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Color(0xFF2864ec),
      child: Column(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '대한민국 1등 홈 서비스\n미소를 만나보세요!',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 30),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 16,
              ),
              ElevatedButton.icon(
                onPressed: () {},
                icon: Icon(
                  Icons.add,
                  color: Colors.blueAccent,
                ),
                label: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 8.0),
                  child: Text(
                    '예약하기',
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    primary: Colors.white),
              )
            ],
          )),
          ElevatedButton(
              onPressed: () {},
              child: Text(
                '서비스 상세정보',
                style: TextStyle(fontWeight: FontWeight.w700),
              )),
          SizedBox(
            height: 32,
          ),
        ],
      ),
    );
  }
}
