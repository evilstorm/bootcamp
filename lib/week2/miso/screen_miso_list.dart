import 'package:flutter/material.dart';

class MisoListScreen extends StatelessWidget {
  const MisoListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 50.0,
              ),
              Text(
                '예약내역',
                style: TextStyle(fontSize: 32, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 50.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.info,
                    color: Colors.blueAccent,
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Text('예약된 서비스가 아직 없어요. 지금 예약해보세요!'),
                ],
              ),
              Divider(
                color: Colors.grey,
              ),
              Spacer(),
              ElevatedButton(
                  onPressed: () {},
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Text(
                      '예약하기',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  )),
            ],
          ),
        ));
  }
}
