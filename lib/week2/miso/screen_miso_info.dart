import 'package:flutter/material.dart';

class MisoInfoScreen extends StatelessWidget {
  const MisoInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 65.0,
            ),
            Text(
              '나의 정보',
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 32.0),
            ),
            SizedBox(
              height: 16.0,
            ),
            Text(
              '010-9999-9999',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            SizedBox(
              height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  infoMenus(Icons.home_outlined, '주소 관리'),
                  infoMenus(Icons.payment_outlined, '결제 수단 관리'),
                  infoMenus(Icons.campaign_outlined, '공지사항'),
                  infoMenus(Icons.help_outline, '문의사항'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Row infoMenus(IconData icon, String title) {
    return Row(
      children: [
        Icon(icon),
        SizedBox(
          width: 32.0,
        ),
        Text(
          title,
          style: TextStyle(fontSize: 16),
        )
      ],
    );
  }
}
