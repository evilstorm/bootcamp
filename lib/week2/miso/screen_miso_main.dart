import 'package:chang_exam/week2/miso/screen_miso_gift.dart';
import 'package:chang_exam/week2/miso/screen_miso_home.dart';
import 'package:chang_exam/week2/miso/screen_miso_info.dart';
import 'package:chang_exam/week2/miso/screen_miso_list.dart';
import 'package:flutter/material.dart';

class MisoMainScreen extends StatefulWidget {
  const MisoMainScreen({Key? key}) : super(key: key);

  @override
  State<MisoMainScreen> createState() => _MisoMainScreenState();
}

class _MisoMainScreenState extends State<MisoMainScreen> {
  int _currentPageIndex = 0;
  final List<Widget> _pageList = [
    MisoHomeScreen(),
    MisoListScreen(),
    MisoGiftScreen(),
    MisoInfoScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _currentPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _pageList[_currentPageIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list,
            ),
            label: 'List',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.redeem,
            ),
            label: 'Gift',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Info',
          ),
        ],
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _currentPageIndex,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      ),
    );
  }
}
