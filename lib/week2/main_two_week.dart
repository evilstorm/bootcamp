import 'package:flutter/material.dart';

class MainTwoWeek extends StatelessWidget {
  const MainTwoWeek({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextButton(
                onPressed: () => Navigator.pushNamed(context, '/miso'),
                child: Text(
                  "Miso 실행",
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pushNamed(context, '/starBucks'),
                child: Text("StarBucks 실행"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
