import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  List<Map<String, dynamic>> dataList = [
    {
      "category": "수제버거",
      "imgUrl":
          "https://i.ibb.co/HBGKYn4/foodiesfeed-com-summer-juicy-beef-burger.jpg",
    },
    {
      "category": "건강식",
      "imgUrl":
          "https://i.ibb.co/mB5YNs2/foodiesfeed-com-pumpkin-soup-with-pumpkin-seeds-on-top.jpg",
    },
    {
      "category": "한식",
      "imgUrl":
          "https://i.ibb.co/Kzzpc97/Beautiful-vibrant-shot-of-traiditonal-Korean-meals.jpg",
    },
    {
      "category": "디저트",
      "imgUrl":
          "https://i.ibb.co/DL5vJVZ/foodiesfeed-com-carefully-putting-a-blackberry-on-tiramisu.jpg",
    },
    {
      "category": "피자",
      "imgUrl": "https://i.ibb.co/qsm8QH4/pizza.jpg",
    },
    {
      "category": "볶음밥",
      "imgUrl":
          "https://i.ibb.co/yQDkq2X/foodiesfeed-com-hot-shakshuka-in-a-cast-iron-pan.jpg",
    },
  ];

  final searchController = TextEditingController();
  final PageController pageController = PageController(
    initialPage: 0,
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                elevation: 0,
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: Icon(
                    Icons.menu,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    _scaffoldKey.currentState!.openDrawer();
                  },
                ),
                actions: [
                  IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.person_outline,
                        color: Colors.black,
                      ))
                ],
                title: Text(
                  "Food Recipe",
                  style: TextStyle(fontSize: 28, color: Colors.black),
                ),
              ),
              drawer: Drawer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    userInfoWidget(),
                    SizedBox(
                      height: 103,
                      child: PageView(
                        controller: pageController,
                        children: [
                          Image.network(
                            "https://i.ibb.co/Q97cmkg/sale-event-banner1.jpg",
                          ),
                          Image.network(
                            "https://i.ibb.co/GV78j68/sale-event-banner2.jpg",
                          ),
                          Image.network(
                            "https://i.ibb.co/R3P3RHw/sale-event-banner3.jpg",
                          ),
                          Image.network(
                            "https://i.ibb.co/LRb1VYs/sale-event-banner4.jpg",
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: menuItem('구매내역'),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: menuItem('저장한 레시피'),
                    ),
                  ],
                ),
              ),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: Expanded(
                      child: TextField(
                        controller: searchController,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            suffixIcon: GestureDetector(
                              onTap: () {},
                              child: Icon(
                                Icons.search,
                                size: 35,
                                color: Colors.black,
                              ),
                            ),
                            hintText: '상품을 검색해주세요.'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: dataList.length,
                        itemBuilder: (context, index) {
                          return listItem(context, index);
                        }),
                  )
                ],
              )),
        ));
  }

  Card listItem(BuildContext context, int index) {
    return Card(
      elevation: 0,
      child: Stack(
        children: [
          SizedBox(
            height: 120,
            width: MediaQuery.of(context).size.width,
            child: ColorFiltered(
              colorFilter:
                  ColorFilter.mode(Color(0x88383838), BlendMode.darken),
              child: Image.network(
                dataList[index]['imgUrl'],
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Positioned.fill(
            child: Center(
              child: Text(
                dataList[index]['category'],
                style: TextStyle(
                    fontSize: 24,
                    color: Colors.white,
                    fontWeight: FontWeight.w800),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Row menuItem(
    String title,
  ) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(fontSize: 16.0),
        ),
        IconButton(onPressed: () {}, icon: Icon(Icons.arrow_forward_ios)),
      ],
    );
  }

  Container userInfoWidget() {
    return Container(
      color: Colors.yellow,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: SizedBox(
                width: 80,
                height: 80,
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(
                        "https://i.ibb.co/CwzHq4z/trans-logo-512.png"),
                  ),
                ),
              ),
            ),
            Text(
              '닉네임',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
            ),
            Text(
              'hello@world.com',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
    );
  }
}
