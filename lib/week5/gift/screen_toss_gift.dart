import 'package:chang_exam/week5/colors.dart';
import 'package:chang_exam/week5/widgets/card_item_type_1.dart';
import 'package:chang_exam/week5/widgets/card_wrapper_container.dart';
import 'package:flutter/material.dart';

class TossGiftScreen extends StatelessWidget {
  TossGiftScreen({Key? key}) : super(key: key);

  final List<dynamic> itemRowList = [
    {'url': "assets/first_page_icon_1.png", 'brief': '만보기', 'title': '140원 받기'},
    {
      'url': "assets/first_page_icon_2.png",
      'brief': '좋아하는 브랜드에서',
      'title': '14540원 받기'
    },
    {
      'url': "assets/first_page_icon_3.png",
      'brief': '행운퀴즈 풀고',
      'title': '랜덤 금액 받기'
    },
    {
      'url': "assets/first_page_icon_4.png",
      'brief': '이번 주 미션하면',
      'title': '20원 받기'
    },
    {
      'url': "assets/first_page_icon_5.png",
      'brief': '머니 알림 받고',
      'title': '500원 받기'
    },
    {
      'url': "assets/first_page_icon_1.png",
      'brief': '카드 등록하고',
      'title': '최대 1억받기'
    },
    {
      'url': "assets/first_page_icon_2.png",
      'brief': '버튼 누르고',
      'title': '10원 주기'
    },
    {'url': "assets/first_page_icon_3.png", 'brief': '택시 타고', 'title': '집에 가기'},
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: TossColor.grey1,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                    alignment: Alignment.centerRight, child: Text('내 쿠폰')),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '해택',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.black,
                            fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      rowItem("assets/first_page_icon_1.png", "내 포인트", "0 원",
                          true, () {}),
                      SizedBox(
                        height: 16.0,
                      ),
                      deleveryBox(context),
                      SizedBox(
                        height: 24.0,
                      ),
                      Text(
                        '포인트 모으기',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.black,
                            fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      ...itemRowList.map(
                        (item) => rowItem(item['url'], item['brief'],
                            item['title'], false, () {}),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  SizedBox deleveryBox(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: CardWrapper(
        backgroundColor: TossColor.grey2,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 24.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/second_page_icon_1.png'),
                SizedBox(
                  height: 4.0,
                ),
                Text(
                  '이번 주 브랜드 도착',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 4.0,
                ),
                Text(
                  '좋아하는 브랜드를 골라보세요',
                  style: TextStyle(fontSize: 12.0, color: Colors.grey),
                ),
                SizedBox(
                  height: 4.0,
                ),
                ElevatedButton(onPressed: () {}, child: Text('10개 브랜드 보기')),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Padding rowItem(String url, String brief, String title, bool titleBlack,
      Function action) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: CardItemType1(
        imageUrl: url,
        imageWidth: 48,
        betweenSpace: 16.0,
        upText: brief,
        bottomText: title,
        bottomColor: titleBlack ? Colors.black : Colors.blue,
        action: IconButton(
          icon: Icon(
            Icons.arrow_forward_ios,
            color: Colors.grey,
          ),
          onPressed: () => action,
        ),
      ),
    );
  }
}
