import 'package:chang_exam/week5/colors.dart';
import 'package:chang_exam/week5/widgets/card_item_type_1.dart';
import 'package:chang_exam/week5/widgets/card_item_type_2.dart';
import 'package:chang_exam/week5/widgets/card_wrapper_container.dart';
import 'package:flutter/material.dart';

class TossHomeScreen extends StatelessWidget {
  const TossHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      color: TossColor.grey1,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(
                      "assets/toss_logo.png",
                      width: 100.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                          "assets/toss_appbar_icon_1.png",
                          width: 50,
                        ),
                        Image.asset(
                          "assets/toss_appbar_icon_2.png",
                          width: 50,
                        ),
                        Image.asset(
                          "assets/toss_appbar_icon_3.png",
                          width: 40,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              CardWrapper(
                child: CardItemType1(
                  imageUrl: 'assets/third_page_icon_1.png',
                  upText: "계좌, 카드 연결이 끊어졌어요",
                  bottomText: '잔액 다시 표시하기',
                  bottomColor: Colors.blue,
                  action: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.arrow_forward_ios),
                  ),
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              CardWrapper(
                title: '토스 뱅크',
                child: CardItemType1(
                  imageUrl: 'assets/third_page_icon_2.png',
                  upText: "토스뱅크 카드 쓰면",
                  bottomText: '매달 초대 40,300원 캐시백',
                  action: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.arrow_forward_ios),
                  ),
                ),
              ),
              SizedBox(
                height: 16.0,
              ),
              sectionAssets(),
              SizedBox(
                height: 16.0,
              ),
              sectionSpend(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: CardWrapper(
                      child: CardItemType2(
                        brief: "1분만에",
                        title: "내 보험\n전부 조회",
                        imageUrl: "assets/third_page_icon_7.png",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Expanded(
                    flex: 1,
                    child: CardWrapper(
                      child: CardItemType2(
                        brief: "혜택 주는",
                        title: "차 보험료\n조회",
                        imageUrl: "assets/third_page_icon_8.png",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Expanded(
                    flex: 1,
                    child: CardWrapper(
                      child: CardItemType2(
                        brief: "최근",
                        title: "캐시백\n받기",
                        imageUrl: "assets/third_page_icon_5.png",
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 24.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      '금액 숨기기',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                    child: VerticalDivider(
                      color: Colors.black,
                      thickness: 1,
                    ),
                  ),
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        '자산 추가',
                        style: TextStyle(color: Colors.black),
                      )),
                ],
              ),
              SizedBox(
                height: 24.0,
              ),
            ],
          ),
        ),
      ),
    ));
  }

  CardWrapper sectionSpend() {
    return CardWrapper(
        title: "소비",
        child: Column(
          children: [
            CardItemType1(
              imageUrl: 'assets/third_page_icon_7.png',
              upText: "이번 달 쓴 금액",
              bottomText: '사용 금액 보기',
              action: ElevatedButton(
                onPressed: () {},
                child: Text('내역'),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            CardItemType1(
              imageUrl: 'assets/third_page_icon_6.png',
              upText: "카드 결제 예정 금액",
              bottomText: '금액 보기',
            ),
          ],
        ));
  }

  CardWrapper sectionAssets() {
    return CardWrapper(
      title: '자산',
      child: Column(
        children: [
          CardItemType1(
            imageUrl: 'assets/third_page_icon_3.png',
            upText: "부산은행 계좌",
            bottomText: '? 원',
            action: ElevatedButton(
              onPressed: () {},
              child: Text('송금'),
            ),
          ),
          SizedBox(
            height: 4.0,
          ),
          CardItemType1(
            imageUrl: 'assets/third_page_icon_4.png',
            upText: "카카오뱅크 계좌",
            bottomText: '? 원',
            action: ElevatedButton(
              onPressed: () {},
              child: Text('송금'),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          CardItemType1(
            imageUrl: 'assets/third_page_icon_5.png',
            upText: "대출 41개 금융사 대기중",
            bottomText: '내 최대 대출 한도 보기',
          ),
        ],
      ),
    );
  }
}
