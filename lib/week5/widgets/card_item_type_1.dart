import 'dart:ffi';

import 'package:flutter/material.dart';

class CardItemType1 extends StatelessWidget {
  final String imageUrl;
  final double imageWidth;
  final double betweenSpace;
  final String upText;
  final String bottomText;
  final Color bottomColor;
  final Widget? action;

  const CardItemType1({
    Key? key,
    required this.imageUrl,
    this.imageWidth = 40.0,
    this.betweenSpace = 8,
    required this.upText,
    required this.bottomText,
    this.bottomColor = Colors.black,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(
          imageUrl,
          width: imageWidth,
        ),
        SizedBox(
          width: betweenSpace,
        ),
        RichText(
          text: TextSpan(
              text: '$upText\n',
              style: TextStyle(color: Colors.grey, fontSize: 12.0),
              children: [
                TextSpan(
                    text: bottomText,
                    style: TextStyle(
                        color: bottomColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0))
              ]),
        ),
        Spacer(),
        Visibility(
          visible: action != null,
          child: action ??
              SizedBox(
                width: 0,
                height: 0,
              ),
        )
      ],
    );
  }
}
