import 'package:flutter/material.dart';

class CardItemType2 extends StatelessWidget {
  final String brief;
  final String title;
  final String imageUrl;
  const CardItemType2(
      {Key? key,
      required this.brief,
      required this.title,
      required this.imageUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          brief,
          style: TextStyle(color: Colors.grey, fontSize: 12.0),
        ),
        SizedBox(
          height: 4.0,
        ),
        Text(
          title,
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w700, height: 1.1),
        ),
        Align(alignment: Alignment.centerRight, child: Image.asset(imageUrl)),
      ],
    );
  }
}
