import 'package:chang_exam/week5/gift/screen_toss_gift.dart';
import 'package:chang_exam/week5/home/screen_toss_home.dart';
import 'package:chang_exam/week5/transfer/screen_toss_transfer.dart';
import 'package:flutter/material.dart';

class TossMainScreen extends StatefulWidget {
  const TossMainScreen({Key? key}) : super(key: key);

  @override
  _TossMainScreenState createState() => _TossMainScreenState();
}

class _TossMainScreenState extends State<TossMainScreen> {
  List<Widget> pages = [
    TossHomeScreen(),
    TossGiftScreen(),
    TossTransferScreen()
  ];

  var _pageIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _pageIndex,
        children: pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black26,
        currentIndex: _pageIndex,
        onTap: (item) {
          setState(() {
            _pageIndex = item;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "홈",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.gif_outlined),
            label: "혜택",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.transfer_within_a_station),
            label: "송금",
          ),
        ],
      ),
    );
  }
}
