import 'dart:convert';

class EventModel {
  String diary;
  DateTime createdAt;
  EventModel({
    required this.diary,
    required this.createdAt,
  });
  void setDiary(String diary) {
    this.diary = diary;
    createdAt = DateTime.now();
  }

  EventModel copyWith({
    String? diary,
    DateTime? createdAt,
  }) {
    return EventModel(
      diary: diary ?? this.diary,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'diary': diary,
      'createdAt': createdAt.millisecondsSinceEpoch,
    };
  }

  factory EventModel.fromMap(Map<String, dynamic> map) {
    return EventModel(
      diary: map['diary'] ?? '',
      createdAt: DateTime.fromMillisecondsSinceEpoch(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  factory EventModel.fromJson(String source) =>
      EventModel.fromMap(json.decode(source));

  @override
  String toString() => 'EventModel(diary: $diary, createdAt: $createdAt)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is EventModel &&
        other.diary == diary &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode => diary.hashCode ^ createdAt.hashCode;
}
