import 'dart:collection';
import 'dart:convert';

import 'package:chang_exam/week3/model_event.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

int getHashCode(DateTime key) {
  return key.day * 1000000 + key.month * 10000 + key.year;
}

class CalendarProvider extends ChangeNotifier {
  final eventMap = HashMap<DateTime, List<EventModel>>(
    equals: isSameDay,
    hashCode: getHashCode,
  );

  List<EventModel> _selectedEvents = [];
  DateTime _selectedDate = DateTime.now();

  List<EventModel> get getSelectedEventList => _selectedEvents;
  DateTime get getSelectedDate => _selectedDate;

  CalendarProvider() {
    loadData();
  }
  EventModel getEventSelectedDateAt(int index) {
    return _selectedEvents[index];
  }

  void setSelectedDate(DateTime selectedDate) {
    _selectedDate = selectedDate;
    _selectedEvents = eventMap[selectedDate] ?? [];
    saveData();

    notifyListeners();
  }

  List<EventModel> eventLoader(DateTime selectedDate) {
    _selectedEvents = eventMap[selectedDate] ?? [];
    return _selectedEvents;
  }

  void addEvent(String diary) {
    if (!eventMap.containsKey(_selectedDate)) {
      eventMap[_selectedDate] = [];
    }
    eventMap[_selectedDate]!
        .add(EventModel(diary: diary, createdAt: DateTime.now()));
    setSelectedDate(_selectedDate);
  }

  void modifyEvent(int index, String diary) {
    eventMap[_selectedDate]![index].setDiary(diary);

    setSelectedDate(_selectedDate);
  }

  void deleteEvent(int index) {
    eventMap[_selectedDate]!.removeAt(index);
    setSelectedDate(_selectedDate);
  }

  void saveData() async {
    final prefs = await SharedPreferences.getInstance();
    Map<String, List<String>> saveMap = Map();
    eventMap.forEach((key, value) {
      if (!saveMap.containsKey('event')) {
        saveMap['event'] = [];
      }
      saveMap['event']!
          .add(jsonEncode({"date": key.toString(), 'events': value}));
    });

    String saveData = jsonEncode({'events': saveMap});
    await prefs.setString('events', saveData);
  }

  void loadData() async {
    final prefs = await SharedPreferences.getInstance();
    var temp = prefs.getString('events');

    var daynaic = jsonDecode(temp!);
    var wapper = daynaic['events'];
    var eventList = wapper['event'];

    eventList.map((data) {
      var jsonData = jsonDecode(data);

      DateTime key = DateTime.parse(jsonData['date'].toString());
      eventMap[key] = [];
      jsonData['events'].map((item) {
        eventMap[key]!.add(EventModel.fromJson(item));
      }).toList();
    }).toList();

    notifyListeners();
  }
}
