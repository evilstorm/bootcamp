import 'package:chang_exam/week3/provider_calendar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class DiaryScreen extends StatefulWidget {
  const DiaryScreen({Key? key}) : super(key: key);

  @override
  _DiaryScreenState createState() => _DiaryScreenState();
}

class _DiaryScreenState extends State<DiaryScreen> {
  final _textController = TextEditingController();
  late CalendarProvider _calendarProvider;

  void _addDiary() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("일기 작성"),
          content: TextField(
            controller: _textController,
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.only(left: 5, bottom: 5, top: 5, right: 5),
                hintText: '한줄 일기를 작성해주세요.'),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("취소"),
              onPressed: () {
                _textController.clear();
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("작성"),
              onPressed: () {
                _calendarProvider.addEvent(_textController.text);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showModifyDialog(int index, String message) {
    _textController.text = message;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("일기 수정"),
          content: TextField(
            controller: _textController,
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.only(left: 5, bottom: 5, top: 5, right: 5),
                hintText: message),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("취소"),
              onPressed: () {
                _textController.clear();
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("수정"),
              onPressed: () {
                _calendarProvider.modifyEvent(index, _textController.text);

                _textController.clear();
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showDeleteDialog(int index, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("일기 삭제"),
          content: Text(
            '"$message"를 삭제하시겠습니까?',
            style: TextStyle(fontSize: 18.0),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("취소"),
              onPressed: () {
                _textController.clear();
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("삭제"),
              onPressed: () {
                _calendarProvider.deleteEvent(index);
                _textController.clear();
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CalendarProvider()),
      ],
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.edit),
          backgroundColor: Colors.blueAccent,
          onPressed: () => _addDiary(),
        ),
        body: SafeArea(
          child:
              Consumer<CalendarProvider>(builder: (context, provider, widget) {
            _calendarProvider = provider;
            return Column(
              children: [
                TableCalendar(
                  firstDay: DateTime.utc(2022, 2, 1),
                  lastDay: DateTime.utc(2022, 2, 28),
                  focusedDay: DateTime.now(),
                  selectedDayPredicate: (day) {
                    return isSameDay(provider.getSelectedDate, day);
                  },
                  onDaySelected: (selectedDay, focusedDay) {
                    provider.setSelectedDate(selectedDay);
                  },
                  eventLoader: provider.eventLoader,
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: provider.getSelectedEventList.length,
                      itemBuilder: (context, index) {
                        final item = provider.getEventSelectedDateAt(index);
                        return GestureDetector(
                          onTap: () => _showModifyDialog(index, item.diary),
                          onLongPress: () =>
                              _showDeleteDialog(index, item.diary),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  item.diary,
                                  style: TextStyle(fontSize: 18.0),
                                ),
                                Text(DateFormat.Hm().format(item.createdAt),
                                    style: TextStyle(fontSize: 12.0)),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
