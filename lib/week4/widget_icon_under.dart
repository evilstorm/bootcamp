import 'package:flutter/material.dart';

class IconUnderTextWidget extends StatelessWidget {
  final IconData icon;
  final String text;
  const IconUnderTextWidget({
    Key? key,
    required this.icon,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        Text(
          text,
          style: TextStyle(color: Colors.white),
        )
      ],
    );
  }
}
