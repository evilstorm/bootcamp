import 'package:chang_exam/week4/screen_shazam_home.dart';
import 'package:flutter/material.dart';

class ShazamLibraryScreen extends StatefulWidget {
  const ShazamLibraryScreen({Key? key}) : super(key: key);

  @override
  _ShazamLibraryScreenState createState() => _ShazamLibraryScreenState();
}

class _ShazamLibraryScreenState extends State<ShazamLibraryScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 40,
          ),
          header(context),
          menuItems(Icons.person, 'Shazam'),
          Divider(),
          menuItems(Icons.access_alarm, '아티스트'),
          Divider(),
          menuItems(Icons.person, '회원님을 위한 재생 목록'),
          SizedBox(
            height: 24.0,
          ),
          Text(
            '최근 Shazam',
            style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700),
          ),
          SizedBox(
            height: 24.0,
          ),
          Expanded(
            child: GridView.builder(
              itemCount: songs.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.64,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
              ),
              itemBuilder: (context, index) {
                return gridItems(songs[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Card gridItems(dynamic data) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.network(data['imageUrl']!),
          SizedBox(
            height: 8.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              data['title']!,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w700),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              data['artist']!,
              style: TextStyle(fontSize: 16.0, color: Colors.grey),
            ),
          ),
          SizedBox(
            height: 24.0,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
            child: Container(
              width: 100,
              height: 40,
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }

  Row menuItems(IconData icon, String menu) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.black,
          size: 28,
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(menu,
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
            )),
      ],
    );
  }

  SizedBox header(BuildContext context) {
    return SizedBox(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Icon(
              Icons.settings,
              size: 32,
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Text(
              '라이브러리',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
          )
        ],
      ),
    );
  }
}
