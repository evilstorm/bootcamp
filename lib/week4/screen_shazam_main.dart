import 'package:chang_exam/week4/widget_icon_under.dart';
import 'package:flutter/material.dart';

class ShazamMainScreen extends StatefulWidget {
  const ShazamMainScreen({Key? key}) : super(key: key);

  @override
  _ShazamMainScreenState createState() => _ShazamMainScreenState();
}

class _ShazamMainScreenState extends State<ShazamMainScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            const Color(0xFF60b2f2),
            const Color(0xFF1947a0),
          ],
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            height: 70,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconUnderTextWidget(icon: Icons.person, text: '라이브러리'),
                IconUnderTextWidget(icon: Icons.chat_rounded, text: '차트'),
              ],
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Text(
            'Shazam하려면 탭하세요',
            style: TextStyle(
                color: Colors.white, fontSize: 24, fontWeight: FontWeight.w700),
          ),
          SizedBox(
            height: 60,
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.lightBlue,
              ),
              width: 200,
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(32.0),
                child: Image.network(
                  'https://cdn.iconscout.com/icon/free/png-256/shazam-3-761709.png',
                  color: Colors.white,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Container(
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
              child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                  )))
        ],
      ),
    );
  }
}
