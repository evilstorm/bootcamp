import 'package:chang_exam/week4/screen_shazam_chart.dart';
import 'package:chang_exam/week4/screen_shazam_library.dart';
import 'package:chang_exam/week4/screen_shazam_main.dart';
import 'package:flutter/material.dart';

const songs = [
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각1',
    'artist': '잔나비',
  },
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각2',
    'artist': '잔나비',
  },
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각',
    'artist': '잔나비',
  },
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각4',
    'artist': '잔나비',
  },
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각5',
    'artist': '잔나비',
  },
  {
    'imageUrl': 'https://i.ytimg.com/vi/jAO0KXRdz_4/hqdefault.jpg',
    'title': '가을밤에 든 생각',
    'artist': '잔나비',
  },
];

class ShazamHomeScreen extends StatefulWidget {
  const ShazamHomeScreen({Key? key}) : super(key: key);

  @override
  _ShazamHomeScreenState createState() => _ShazamHomeScreenState();
}

class _ShazamHomeScreenState extends State<ShazamHomeScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  Color _indecatorColor = Colors.white;
  Color _indecatorBgColor = Color(0xFF60b2f2);

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 3, initialIndex: 0, vsync: this);
    _tabController?.addListener(() {
      setState(() {
        if (_tabController!.index == 1) {
          _indecatorColor = Colors.white;
          _indecatorBgColor = Color(0xFF60b2f2);
        } else {
          _indecatorColor = Colors.blue;
          _indecatorBgColor = Color(0xFF383838);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: Stack(
            children: [
              Positioned.fill(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    ShazamLibraryScreen(),
                    ShazamMainScreen(),
                    ShazamChartScreen(),
                  ],
                ),
              ),
              Positioned.fill(
                top: 80,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: TabPageSelector(
                    controller: _tabController,
                    selectedColor: _indecatorColor,
                    color: _indecatorBgColor,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
