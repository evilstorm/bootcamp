import 'package:chang_exam/week6/model_book.dart';
import 'package:chang_exam/week6/provider_book.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class BookStoreScreen extends StatefulWidget {
  const BookStoreScreen({Key? key}) : super(key: key);

  @override
  State<BookStoreScreen> createState() => _BookStoreScreenState();
}

class _BookStoreScreenState extends State<BookStoreScreen> {
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => BookProvider(),
        ),
      ],
      child: Consumer<BookProvider>(builder: (context, provider, widget) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 2,
            title: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Book Store',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
            bottom: PreferredSize(
              preferredSize: Size(0, 60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                    child: Text('total: ${provider.bookInfo?.totalItems ?? 0}'),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32.0, vertical: 8.0),
                    child: TextField(
                      controller: _searchController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                          hintText: "책을 검색해주세요.",
                          suffixIcon: IconButton(
                            icon: Icon(Icons.search),
                            onPressed: () {
                              if (_searchController.text.isNotEmpty) {
                                provider
                                    .getBookInfo(_searchController.text.trim());
                              }
                            },
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
          body: SafeArea(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: Column(
                children: [
                  SizedBox(
                    height: 16.0,
                  ),
                  Expanded(
                    child: ListView.separated(
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 8.0,
                      ),
                      itemCount: provider.bookInfo?.items?.length ?? 0,
                      itemBuilder: (BuildContext context, int index) {
                        final book = provider.bookInfo?.items?[index];
                        return listItems(book);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  GestureDetector listItems(Items? book) {
    return GestureDetector(
      onTap: () {
        print('URL: ${book?.volumeInfo?.previewLink}');
        launch(book?.volumeInfo?.previewLink ?? 'https://www.google.com');
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            SizedBox(
              width: 80,
              height: 120,
              child: Image.network(book?.volumeInfo?.imageLinks?.thumbnail ??
                  'http://phone.ybmclass.com/common_css/img/noimage.gif'),
            ),
            SizedBox(
              width: 16.0,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      book?.volumeInfo?.title ?? 'UnKnown',
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      book?.volumeInfo?.subtitle ?? '',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
