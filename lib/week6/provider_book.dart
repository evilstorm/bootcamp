import 'package:chang_exam/week6/model_book.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class BookProvider extends ChangeNotifier {
  BookInfo? bookInfo;

  void getBookInfo(String target) async {
    var response = await Dio()
        .get('https://www.googleapis.com/books/v1/volumes?q=${target}');

    bookInfo = BookInfo.fromJson(response.data);

    notifyListeners();
  }
}
